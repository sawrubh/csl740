<?php
	$mylat = $_POST['latitude'];
	$mylon = $_POST['longitude'];
	$Category = $_POST['sel1'];
	$Subcategory = $_POST['sel2'];
	$Distance = $_POST['distance'];
	include 'config.inc.php';

	$Subcategory = strtolower($Subcategory);

	switch($Category)
	{
		case 'Travel':
			$ctable = 'travel';
			break;
		case 'Restaurant':
			$ctable = 'restaurant';
			break;
		case 'Shopping':
			$ctable = 'shopping';
			break;
		case 'Recreation':
			$ctable = 'recreation';
			break;
		case 'Entertainment':
			$ctable = 'entertainment';
			break;
		case 'Education':
			$ctable = 'education';
			break;
		default:
			$ctable = 'places';
	}
	$lat1 = $mylat-($Distance/69);
	$lat2 = $mylat+($Distance/69);
	$lon1 = $mylon-$Distance/abs(cos(deg2rad($mylat))*69);
	$lon2 = $mylon+$Distance/abs(cos(deg2rad($mylat))*69);
	if($Subcategory=='' or $Subcategory='select')
	{
		$t2 = "SELECT name,address1,address2,locality,region,website,email,latitude,longitude,tel,fax,categry2,categry1,
		3956 * 2 * ASIN(SQRT( POWER(SIN((".$mylat." - ".$ctable.".latitude)
		* pi()/180 / 2), 2) +
		COS(".$mylat." * pi()/180) * COS(".$ctable.".latitude * pi()/180) *
		POWER(SIN((".$mylon." - ".$ctable.".longitude) * pi()/180 / 2), 2) )) as
		distance FROM ".$ctable."
		WHERE ".$ctable.".longitude between ".$lon1." and ".$lon2."
		and ".$ctable.".latitude between ".$lat1." and ".$lat2."
		having distance < ".$Distance." ORDER BY Distance limit 100";
	}
	else
	{
		$t2 = "SELECT name,address1,address2,locality,region,website,email,latitude,longitude,tel,fax,categry2,categry1,
		3956 * 2 * ASIN(SQRT( POWER(SIN((".$mylat." - ".$ctable.".latitude)
		* pi()/180 / 2), 2) +
		COS(".$mylat." * pi()/180) * COS(".$ctable.".latitude * pi()/180) *
		POWER(SIN((".$mylon." - ".$ctable.".longitude) * pi()/180 / 2), 2) )) as
		distance FROM ".$ctable."
		WHERE ".$ctable.".longitude between ".$lon1." and ".$lon2."
		and ".$ctable.".latitude between ".$lat1." and ".$lat2." and categry2 = '".$Subcategory."' 
		having distance < ".$Distance." ORDER BY Distance limit 100";
	}

	$result = mysqli_query($dbconn, $t2);	
	$List = array();
while($row = mysqli_fetch_row($result))
{
	$temp = array($row[0],$row[1]." ".$row[2],$row[3],$row[5],$row[6],$row[7],$row[8],$row[9],$row[10],$row[11],$row[12]);
	$List[] = $temp;
}
mysqli_close($dbconn);
$current = 0;
$initial = $current;
$length = count($List);
$maxData = 12;

$res = '';
if($length - $current > 0)
{
	$i=1;
	while($i <= $maxData and $current < $length)
	{
		$res = $res . $List[$current][0] . ',' . $List[$current][1] . ',' . $List[$current][2] . ',' . $List[$current][9] .'%%' ;
		$current = $current + 1;
		$i=$i+1;
	}
}
				
session_start();	
$_SESSION["ini"] = $initial;
$_SESSION["cur"] = $current;
$_SESSION["arr"] = $List;
$_SESSION["len"] = $length;
$_SESSION["max"] = $maxData;

?>

<html>
<head>
	<link rel="stylesheet" href="style.css">
	<script>
	
	function capitalize(A)
		{
			var temp = A.split(' ');
			var n  = temp.length;
			var i=0;
			var res = '';
			while(i<n)
			{				
				res = res + temp[i].substring(0,1).toUpperCase() + temp[i].substring(1,temp[i].length) + ' ';
				i = i + 1;
			}
			return res;
		}
		
	function initialize()
	{
		var res = "<?php echo $res; ?>";
		var list = res.split('%%');
		var len = list.length;
		document.getElementById("table-2").innerHTML += ' <tr> <th> Name </th><th> Address </th> <th> City </th> <th> Category </th> </tr>';
		if(len>0)
		{
			for(i = 0; i<len-1; i++)
			{
				var attri = list[i].split(',');
				attri[0] = capitalize(attri[0]);
				attri[1] = capitalize(attri[1]);
				attri[2] = capitalize(attri[2]);
				attri[3] = capitalize(attri[3]);
				document.getElementById("table-2").innerHTML += ' <tr> <td>' + '<a href="abc.php?integ='+i+'">'+attri[0]+'</a>' + '</td><td>' + attri[1] + '</td> <td>' + attri[2] +  '</td> <td>' + attri[3]+  '</td> </tr>';
			}
		}
		document.getElementById("table-2").innerHTML += ' <tr> <td></td><td></td> <td></td> <td> <a href="newr.php" >Next...</a> </td> </tr>';
	}
	</script>
</head>

<body onload="initialize()">

<div id="toplevel"> </div>	
<div id="toplevel">
	</div>
		<div id = "toplevelBJS">
		<br />
		<p> <font size="6" color = "white">CSL740</font>  </p>
		</div>
		<div id="SA">
		<br />
			<p> <font size="5" color = "white"> <a href="home.php">Search Area</a></font>  </p>
		</div>
		
		<div id="RS">
			<p> <font size="5" color = "white"> <a href="rs.php"> Relative Search </a></font>  </p>
		</div>
		
		<div id="AAP">
			<p> <font size="5" color = "white"><a href="aap.php">Around A point  </a></font>  </p>
		</div>

		<div id="AD">
			<p> <font size="5" color = "white"><a href="ad.php">Search Events  </a></font>  </p>
		</div>
		
		<?php
		session_start();
		if (isset($_SESSION['name'])) {
		echo "<div id='LO'>
			    <p> <font size='5' color = 'white'> <a href='logout.php'>Logout</a></font>  </p>
        	  </div>";
		} else {
		echo "<div id='LI'>
			    <p> <font size='5' color = 'white'> <a href='login.php'>Login</a></font>  </p>
        	  </div>";
		}
		?>
    		
		
<table id="table-2">
</table>

</body>
