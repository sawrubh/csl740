<?php


	session_start();
	$ind = $_GET["ind"];
	$data = $_SESSION["data"];
	$row = $data[$ind];
	$title = $row[0];
	$description = $row[1];
	$state = $row[2];
	$city = $row[3];
	$lat = $row[4];
	$lon = $row[5];
	$sdate = $row[6];
	$edate = $row[7];
	

?>

<html>
  <head>
	<link rel="stylesheet" href="style.css">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body
	  { 
					color: #262626;
					background:fixed center repeat-x;
					background-image: url(back.jpg);
					font: normal 12px/18px Verdana, sans-serif;
					background-size:1000px 800px
	  }
	  fieldset { margin: 0 0 22px 0; border: 1px solid #2B6600; padding: 12px 17px; background-color: #DAF5C7; }
	  legend { font-size: 1.1em; background-color: #2B6600; color: #FFFFFF; font-weight: bold; padding: 4px 8px; }

form br { display: inline; }
	
	#toplevel
	{
		position:absolute;
			top:0px;
			left:0px;
			width:100%; 
			height:15%; 
			border: 2px solid black;
			 background:fixed center;
			background-image: url(blue.png);
	}
	#toplevelBJS
	{
		position:absolute;
		top:10px;
		left:10px;
		font-family: "New Century Schoolbook", Times, serif;
		
	}
	#SA
	{
		position:absolute;
		top:10px;
		left:200px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#RS
	{
		position:absolute;
		top:30px;
		left:500px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#AAP
	{
		position:absolute;
		top:30px;
		left:800px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#EC
	{
		position:absolute;
		top:30px;
		left:1050px;
		font-family: "New Century Schoolbook", Times, serif;
	}
      #map_canvas 
		{
			position:absolute;
			top:110px;
			left:0px;
			width:800px; 
			height:80%; 
			border: 2px solid black;
		}
      #search_area 
		{	 
			background-image: url(green.jpg);
		}
		
	#stylized
	{
			position: absolute;
			top:100px;
			left:800px;
			width : 500px;
			height:80%;
			background-image: url(green.jpg);
			margin: 10px 10px 10px 10px; 
			border: 2px solid black; 
}

      #search_relative_1 {position: absolute; float; width = 40%; right: 40px ; top:370px; margin: 10px 10px 10px 10px; border: 2px solid black; }
      #search_relative_2 {position: absolute; float; width = 40%; right: 80px ; top:640px; margin: 10px 10px 10px 10px; border: 2px solid black;}
    </style>
	
	 <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC8NGJA6xF3MC8UxNVY1hrK8C1P9LW8has&sensor=true">
    </script>
	<script type="text/javascript">
		
		
		function capitalize(A)
		{
			var temp = A.split(' ');
			var n  = temp.length;
			var i=0;
			var res = '';
			while(i<n)
			{				
				res = res + temp[i].substring(0,1).toUpperCase() + temp[i].substring(1,temp[i].length) + ' ';
				i = i + 1;
			}
			return res;
		}
		
		function trim(A)
		{
			return A.replace(/^\s+|\s+$/g,"");
		}
      function initialize() 
	  {
		var title = "<?php echo $title ?>";
		var description = "<?php echo $description ?>";
		var state = "<?php echo $state ?>";
		var city = "<?php echo $city ?>";
		var lat = "<?php echo $lat ?>";
		var lon = "<?php echo $lon ?>";
		var sdate = "<?php echo $sdate ?>";
		var edate = "<?php echo $edate ?>";
		var lat = parseFloat(lat);
		var lon = parseFloat(lon);
		
		if(!(title == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> Title: ' + title +'</font></p></center><br />';
		}
		if(!(description == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> Description: ' + description +'</font></p></center>';
		}
		if(!(state == ''))
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> State: ' + state +'</font></p></center>';
		if(!(city == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> City: ' + city +'</font></p></center>';
		}		
		if(!(lat == '' || lon == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> Lat-Long: ' + lat +', '+lon+'</font></p></center>';
		}
		if(!(sdate == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> Start Date: ' + sdate +'</font></p></center>';
		}
		if(!(edate == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> End Date: ' + edate +'</font></p></center>';
		}		
		
	    var myOptions = {
          center: new google.maps.LatLng(lat,lon),
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          styles: [
		  {
		    featureType: "administrative",
		    elementType: "labels",
		    stylers: [
		      { visibility: "on" }
		    ]
		  },{
		    featureType: "landscape",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "poi",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "road",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "transit",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "water",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  }
		]
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"),
            myOptions);
			

	 var marker = new google.maps.Marker({
	    position: map.getCenter(),
	    map: map,
	    title: 'Click to zoom'
	  });
	  }
	   </script>
	   
	   </head>
   <body onload="initialize()">
		  <div id="map_canvas" ></div>
	<div id="toplevel">
	</div>
		<div id = "toplevelBJS">
		<br />
		<p> <font size="6" color = "white"> CSL740 </font>  </p>
		</div>
		<div id="SA">
		<br />
			<p> <font size="5" color = "white"> <a href="home.php">Search Area</a></font>  </p>
		</div>
		
		<div id="RS">
			<p> <font size="5" color = "white"> <a href="rs.php"> Relative Search </a></font>  </p>
		</div>
		
		<div id="AAP">
			<p> <font size="5" color = "white"><a href="aap.php">Around A point  </a></font>  </p>
		</div>
		
		<div id="stylized"> 
		
		
		</div>
		
		
		 </body>
</html>
