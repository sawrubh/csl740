<!DOCTYPE html>
<html>
  <head>
    <!-- Bootstrap core CSS -->
    <link href="signin.css" rel="stylesheet">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
    fieldset { margin: 0 0 22px 0; border: 1px solid #2B6600; padding: 12px 17px; background-color: #DAF5C7; }
	  legend { font-size: 1.1em; background-color: #2B6600; color: #FFFFFF; font-weight: bold; padding: 4px 8px; }

form br { display: inline; }
	 
	 #toplevel
	{
		position:absolute;
			top:0px;
			left:0px;
			width:100%; 
			height:15%; 
			border: 2px solid black;
			 background:fixed center;
			background-image: url(blue.png);
	}
	#toplevelBJS
	{
		position:absolute;
		top:10px;
		left:10px;
		font-family: "New Century Schoolbook", Times, serif;
		
	}
	#SA
	{
		position:absolute;
		top:10px;
		left:150px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#RS
	{
		position:absolute;
		top:30px;
		left:350px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#AAP
	{
		position:absolute;
		top:30px;
		left:550px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#AD
	{
		position:absolute;
		top:30px;
		left:800px;
		font-family: "New Century Schoolbook", Times, serif;
	}	
      #search_area 
		{	 
			background-image: url(green.jpg);
		}
		
	#stylized
	{
			position: absolute;
			top:100px;
			left:800px;
			width : 500px;
			height:80%;
			background-image: url(green.jpg);
			margin: 10px 10px 10px 10px; 
			border: 2px solid black; 
}
</style>
   </head>

  <body>

	<div id="toplevel">
	</div>
		<div id = "toplevelBJS">
		<br />
		<p> <font size="6" color = "white">CSL740</font>  </p>
		</div>
		<div id="SA">
		<br />
			<p> <font size="5" color = "white"> <a href="home.php">Search Area</a></font>  </p>
		</div>
		
		<div id="RS">
			<p> <font size="5" color = "white"> <a href="rs.php"> Relative Search </a></font>  </p>
		</div>
		
		<div id="AAP">
			<p> <font size="5" color = "white"><a href="aap.php">Around A point  </a></font>  </p>
		</div>
        
        <div id="AD">
			<p> <font size="5" color = "white"><a href="ad.php">Search Events  </a></font>  </p>
		</div>
        </div>
    <div class="container">
      <form class="form-signin" method = "POST" action = "">
        <input type="email" class="form-control" name = "email" placeholder="Email address" autofocus required>
        <input type="password" class="form-control" name = "password"  placeholder="Password" required>
        <button name="submit" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

      <div id = "signup">
        <p>Or you could register<a href="signup.php"> here </a>
      </div>

    </div> <!-- /container -->

    <?php

      include 'config.inc.php';

      if (isset($_POST['submit'])) {
        $email = $_POST['email'];
        $pass = $_POST['password'];
        $email = mysqli_real_escape_string($dbconn, trim($_POST['email']));

        $query = "SELECT pass_hash, salt, name, id FROM users WHERE user_email = '$email'";
        $result = mysqli_query($dbconn, $query) OR die('error in query');
        $num = mysqli_num_rows($result);
        if ($num < 1) {
          echo "Incorrect email or password. Please verify the data you entered\n";
        } else {
          session_start();
          $_SESSION['email'] = $_POST['email'];
          $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
          $_SESSION['name'] = $row['name'];
          $_SESSION['user_id'] = $row['id'];
          $hash = crypt($pass, '$2a$12$'.$row['salt']);
          if ($hash == $row['pass_hash']) {
            header('Location: home.php');
          } else {
            echo "Incorrect email or password. Please verify the data you entered\n";
          }
        }
      }
    ?>

  </body>
</html>
