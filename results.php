<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { margin: 10px 10px 10px 10px; padding: 0 }
      #map_canvas {width:800px; height:600px; border: 2px solid black;}
      #result_area { position: absolute; width = 40%; right: 200px ; top:0px; margin: 10px 10px 10px 10px; border: 2px solid black; }
    </style>
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC8NGJA6xF3MC8UxNVY1hrK8C1P9LW8has&sensor=true">
    </script>
    <script type="text/javascript">
      function initialize() {

        var myOptions = {
          center: new google.maps.LatLng(45.397, 270.644),
          zoom: 4,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          styles: [
		  {
		    featureType: "administrative",
		    elementType: "labels",
		    stylers: [
		      { visibility: "on" }
		    ]
		  },{
		    featureType: "landscape",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "poi",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "road",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "transit",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "water",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  }
		]
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"),
            myOptions);

	 var marker = new google.maps.Marker({
	    position: map.getCenter(),
	    map: map,
	    title: 'Click to zoom'
	 });
	//code to generate table from the array just received via php processing
	var i = 0; var query_result = new Array(); var str1 = "";
	query_result[1] = 'amsamd';
	query_result[2] = 'bndsn';
	query_result[3] = 'cerfef';
	query_result[4] = 'deferf';
	query_result[0] = 'edefer';

	for(i=0; i<query_result.length; i++)
	{
	document.getElementById("table1").innerHTML+=str1.concat('<tr>','<td>',query_result[i],'</td>','<td>',query_result[i],'</td>','</tr>','<br>');
	};
     }

    </script>

   </head>
   <body onload="initialize()">
   <div id="map_canvas"></div>
   <div id="result_area">
   <table id="table1">

   </table>
   </div>
   </body>
 </html>
