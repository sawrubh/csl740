<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<style type="text/css">
	 html { height: 100% }
         body 
	  { 
					color: #262626;
					background:fixed center repeat-x;
					background-image: url(back.jpg);
					font: normal 12px/18px Verdana, sans-serif;
					background-size:1000px 800px
	  }

 fieldset { margin: 0 0 22px 0; border: 1px solid #2B6600; padding: 12px 17px; background-color: #DAF5C7; }
	  legend { font-size: 1.1em; background-color: #2B6600; color: #FFFFFF; font-weight: bold; padding: 4px 8px; }

form br { display: inline; }
	 
	 #toplevel
	{
		position:absolute;
			top:0px;
			left:0px;
			width:100%; 
			height:15%; 
			border: 2px solid black;
			 background:fixed center;
			background-image: url(blue.png);
	}
	#toplevelBJS
	{
		position:absolute;
		top:10px;
		left:10px;
		font-family: "New Century Schoolbook", Times, serif;
		
	}
	#SA
	{
		position:absolute;
		top:10px;
		left:150px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#RS
	{
		position:absolute;
		top:30px;
		left:350px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#AAP
	{
		position:absolute;
		top:30px;
		left:550px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#AD
	{
		position:absolute;
		top:30px;
		left:800px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#LO
	{
		position:absolute;
		top:30px;
		left:1050px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#LI
	{
		position:absolute;
		top:30px;
		left:1050px;
		font-family: "New Century Schoolbook", Times, serif;
	}	
     #map_canvas 
		{
			position:absolute;
			top:110px;
			left:0px;
			width:800px; 
			height:80%; 
			border: 2px solid black;
		}
      #search_area 
		{	 
			background-image: url(green.jpg);
		}
		
	#stylized
	{
			position: absolute;
			top:100px;
			left:800px;
			width : 500px;
			height:80%;
			background-image: url(green.jpg);
			margin: 10px 10px 10px 10px; 
			border: 2px solid black; 
}
	</style>

   <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC8NGJA6xF3MC8UxNVY1hrK8C1P9LW8has&sensor=true">
    </script>

   <script type="text/javascript">
      var geocoder;
      var infowindow = new google.maps.InfoWindow();
      var map;
      var marker;
      
      function initialize() {
	   var temp = "<?php 
    include 'config.inc.php';
		if (mysqli_connect_errno())
		{
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	      $query = "select name from state";
	      $result = mysqli_query($dbconn, $query);
	      $temp = "";
	      $flag = 0;
	      while($row = mysqli_fetch_row($result))
	      {
	      if($flag ==0 )
	      {
	      $flag=1;
	      $temp = "$row[0]";
	      }
	      else
	      $temp = $temp.","."$row[0]";
	      }
	      mysqli_close($dbconn);
	      echo $temp;
	      ?>";
	      var arr = temp.split(',');
	      var n = arr.length;
	      var i=0;
	      for(i=0;i<n;i++)
		  {
		      document.forms["form2"]["sel3"].options[i+1]=new Option(arr[i],arr[i]);
		  }
 	  
	  var myOptions = {
          center: new google.maps.LatLng(45.397, 270.644),
          zoom: 4,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          styles: [
		  {
		    featureType: "administrative",
		    elementType: "labels",
		    stylers: [
		      { visibility: "on" }
		    ]
		  },{
		    featureType: "landscape",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "poi",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "road",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "transit",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "water",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  }
		]
        };

	  map = new google.maps.Map(document.getElementById("map_canvas"),
            myOptions);

   marker = new google.maps.Marker({
	    position: map.getCenter(),
	    map: map,
	    draggable:true,
    	animation: google.maps.Animation.DROP,
        title: 'Your Event',
	  });

    geocoder = new google.maps.Geocoder();
     codeLatLng(45.397, 270.644);
    	
   var clickListen = google.maps.event.addListener(marker, "dragend", function(event)
    {
    	var x_coord = event.latLng.lat();
    	var y_coord = event.latLng.lng();
        codeLatLng(x_coord, y_coord);
    	document.forms["form3"]['lat'].value = event.latLng.lat();
    	document.forms["form3"]['long'].value = event.latLng.lng();
    });
    
}

function change_div(){
    document.getElementById('search_div').style.visibility = "hidden";
    document.getElementById('add_div').style.visibility = "visible";
}

function codeLatLng(lat, lng) {
  geocoder.geocode({'latLng': new google.maps.LatLng(lat, lng)}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[1]) {
      infowindow.setContent(results[1].formatted_address);
        infowindow.open(map, marker);
for (i = 0; i < results[1].address_components.length; i++){
if (results[1].address_components[i].types[0] == 'administrative_area_level_1') {
    document.forms["form3"]['state'].value = results[1].address_components[i].long_name;
}
if (results[1].address_components[i].types[0] == ('administrative_area_level_3' || 'administrative_area_level_2')) {
    document.forms["form3"]['city'].value = results[1].address_components[i].long_name;
}
}
      } else {
        alert('No results found');
      }
    } else {
      alert('Geocoder failed due to: ' + status);
    }
  });
}
   </script>
   
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js">
</script>

    <script language="JavaScript" type="text/javascript">
        $( document ).ready(function() {
        $( "#title4" ).change(function () {
            marker.title = document.forms["form3"]["title"].value;
          });
        });

   function checkSel(selName,selIndex,val){
		  if(selName=="sel3"&&selIndex!=0){
				  document.forms["form2"]['sel'+val].disabled=false;
				  
				  
				  
				  
				  var ajaxRequest;  // The variable that makes Ajax possible!
				  
				  try{
						  // Opera 8.0+, Firefox, Safari
					  ajaxRequest = new XMLHttpRequest();
				  } catch (e){
						  // Internet Explorer Browsers
					  try{
						  ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
					  } catch (e) {
						  try{
							  ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
						  } catch (e){
								  // Something went wrong
							  alert("Your browser broke!");
							  return false;
						  }
					  }
				  }
				  
					  // Create a function that will receive data sent from the server
				  ajaxRequest.onreadystatechange = function()
			      {
				  if(ajaxRequest.readyState == 4){
						  //document.myForm.time.value = ajaxRequest.responseText;
						  //put the code for updating the data received from the server
					  var temp = ajaxRequest.responseText;
					  var arr = temp.split(',');
					  var n = arr.length;
					  var i=0;
					  for(i=0;i<n;i++)
					      {
						  document.forms["form2"]["sel4"].options[i+1]=new Option(arr[i],arr[i]);
					      }	
				  }
			      }
				  var t = document.forms["form2"]['sel3'].value;
				  ajaxRequest.open("GET", "city_list.php?state="+t, true);
					  //	    var a = {'state': document.forms["form1"]['sel'+val].value};
				  ajaxRequest.send(null); 
				  
			  }
			  if(selName=="sel3"&&selIndex==0){
				  document.forms["form2"]['sel'+val].disabled=true;
				  document.forms["form2"]['sel'+val].selectedIndex=0;
			  }
	
	}	
   </script>
 
</head>
<body onload="initialize()">
	
	<div id="map_canvas" ></div>	
	<div id="toplevel">
	</div>
		<div id = "toplevelBJS">
		<br />
		<p> <font size="6" color = "white">CSL740</font>  </p>
		</div>
		
		<div id="SA">
		<br />
			<p> <font size="5" color = "white"> <a href="home.php">Search Area</a></font>  </p>
		</div>
		
		<div id="RS">
			<p> <font size="5" color = "white"> <a href="rs.php"> Relative Search </a></font>  </p>
		</div>
		
		<div id="AAP">
			<p> <font size="5" color = "white"><a href="aap.php">Around A point  </a></font>  </p>
		</div>

		<div id="AD">
			<p> <font size="5" color = "white"><a href="ad.php">Search Events  </a></font>  </p>
		</div>
		
		<?php
		session_start();
		if (isset($_SESSION['name'])) {
        $firstName = explode(' ', trim($_SESSION['name']));
		echo "<div id='LO'>
			    <p> <font size='5' color = 'white'> Yo, ".$firstName[0]." <a href='logout.php'>Logout</a></font>  </p>
        	  </div>";
		} else {
		echo "<div id='LI'>
			    <p> <font size='5' color = 'white'> <a href='login.php'>Login</a></font>  </p>
        	  </div>";
		}
		?>

<div id="stylized">
<div id="search_div">
<form id="form2" method="post" action="search-events.php">
	<fieldset>
	
			<legend>Search An Event:</legend>	
			<div>
			<b>Category Tags: </b>
			  <input type="checkbox" name="cat[]" value="Food" >Food
			   <input type="checkbox" name="cat[]" value="Music" >Music
			  <input type="checkbox" name="cat[]" value="Education" >Education 
			   <input type="checkbox" name="cat[]" value="Entertainment" >Entertainment 
			   <input type="checkbox" name="cat[]" value="Carnival" >Carnival &nbsp 
			   <input type="checkbox" name="cat[]" value="Exhibition" >Exhibition 
			   <input type="checkbox" name="cat[]" value="Party">Party 
			   <input type="checkbox" name="cat[]" value="Concert">Concert 
			   <input type="checkbox" name="cat[]" value="Art">Art 
			   <input type="checkbox" name="cat[]" value="Dance">Dance 
			  <input type="checkbox" name="cat[]" value="Miscellaneous">Miscellaneous 
			<br />		
			<br />					

			<b>State: </b><select name="sel3" onchange="checkSel(this.name,this.selectedIndex,4)" >
			<option>select</option>
			<option>blah2_1</option>
			<option>blah2_2</option>
			<option>blah2_3</option>
			</select><br />
				<br />					

			<b>City: </b><select name="sel4" disabled >
			<option>select</option>
			<option>blah2_1</option>
			<option>blah2_2</option>
			<option>blah2_3</option>
			</select><br />
				<br />					

			<b>Search till: </b><select name="sel5">
			<option>Today</option>
			<option>Tomorrow</option>
			<option>No Limit</option>
			</select><br />	<br />					

			</div>

			
	</fieldset>

   <input type="submit" />
</form>
        <?php
        if ($_SESSION['name']) {
            echo "<a style ='color: white' onclick='change_div()' href='#' >Click here to Add your own Event</a></br></br>";
        } else {
            echo '<a style ="color: white; margin-bottom:5px;" href="login.php" >Click here to login and Add your own Event</a>';
        }
        ?>
</div>

<div id="add_div" style="margin-top:-320px;visibility: hidden">
<form id="form3" method="post" onsubmit="alert(document.forms['form3']['start_date'].value)" action="add-events.php">
	<fieldset>
			<legend>Add Your Own:</legend>		
			<b>Title:  <input type="text" name="title" id="title4" "size="35" maxlength="200" placeholder="Your Event"><br />
			<br />			
	
		<textarea name="description" rows="6" cols="50" placeholder="Description(optional)" ></textarea><br />
			<br />
		
		<input type="hidden" name="lat" value="45.397">
		<input type="hidden" name="long" value="270.644">
		<input type="hidden" name="city" value="Parrish">
		<input type="hidden" name="state" value="Wisconsin">
		
		<b>Category Tags: </b>
		  <input type="checkbox" name="cat[]" value="Food" >Food
		   <input type="checkbox" name="cat[]" value="Music" >Music
		  <input type="checkbox" name="cat[]" value="Education" >Education 
		   <input type="checkbox" name="cat[]" value="Entertainment" >Entertainment 
		   <input type="checkbox" name="cat[]" value="Carnival" >Carnival &nbsp 
		   <input type="checkbox" name="cat[]" value="Exhibition" >Exhibition 
		   <input type="checkbox" name="cat[]" value="Party">Party 
		   <input type="checkbox" name="cat[]" value="Concert">Concert 
		   <input type="checkbox" name="cat[]" value="Art">Art 
		   <input type="checkbox" name="cat[]" value="Dance">Dance 
		  <input type="checkbox" name="cat[]" value="Miscellaneous">Miscellaneous 
			<br />		
			<br />					
			
		<b>Start Date:  </b><input type="datetime-local" name="start_date"><br />
			<br />
	   
	    <b>End Date:  </b><input type="datetime-local" name="end_date"><br />
			<br />
	
		
	</fieldset>
  
  
    
    <input type="submit">
</form>
</div>
</div>

</body>
</html>
