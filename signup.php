<!DOCTYPE html>
<html>
  <head>
    <!-- Bootstrap core CSS -->
    <link href="signin.css" rel="stylesheet">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
    fieldset { margin: 0 0 22px 0; border: 1px solid #2B6600; padding: 12px 17px; background-color: #DAF5C7; }
	  legend { font-size: 1.1em; background-color: #2B6600; color: #FFFFFF; font-weight: bold; padding: 4px 8px; }

form br { display: inline; }
	 
	 #toplevel
	{
		position:absolute;
			top:0px;
			left:0px;
			width:100%; 
			height:15%; 
			border: 2px solid black;
			 background:fixed center;
			background-image: url(blue.png);
	}
	#toplevelBJS
	{
		position:absolute;
		top:10px;
		left:10px;
		font-family: "New Century Schoolbook", Times, serif;
		
	}
	#SA
	{
		position:absolute;
		top:10px;
		left:150px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#RS
	{
		position:absolute;
		top:30px;
		left:350px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#AAP
	{
		position:absolute;
		top:30px;
		left:550px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#AD
	{
		position:absolute;
		top:30px;
		left:800px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#LO
	{
		position:absolute;
		top:30px;
		left:1050px;
		font-family: "New Century Schoolbook", Times, serif;
	}	
      #search_area 
		{	 
			background-image: url(green.jpg);
		}
		
	#stylized
	{
			position: absolute;
			top:100px;
			left:800px;
			width : 500px;
			height:80%;
			background-image: url(green.jpg);
			margin: 10px 10px 10px 10px; 
			border: 2px solid black; 
}
</style>
  </head>

  <body>
	<div id="toplevel">
	</div>
		<div id = "toplevelBJS">
		<br />
		<p> <font size="6" color = "white">CSL740</font>  </p>
		</div>
		<div id="SA">
		<br />
			<p> <font size="5" color = "white"> <a href="home.php">Search Area</a></font>  </p>
		</div>
		
		<div id="RS">
			<p> <font size="5" color = "white"> <a href="rs.php"> Relative Search </a></font>  </p>
		</div>
		
		<div id="AAP">
			<p> <font size="5" color = "white"><a href="aap.php">Around A point  </a></font>  </p>
		</div>
        
        <div id="AD">
			<p> <font size="5" color = "white"><a href="ad.php">Add Your Events  </a></font>  </p>
		</div>
		
		<div id="LO">
			<p> <font size="5" color = "white"> <a href="login.php">Login</a></font>  </p>
		</div>
        </div>
    <div class="container">
      <form class="form-signin" method = "POST" action="">
        <h2 class="form-signin-heading">Sign Up</h2>
        <input type="text" class="form-control" name = "name" placeholder="Full Name" autofocus required>
        <input type="email" class="form-control" name = "email" placeholder="Email address" >
        <input type="password" class="form-control" name = "pass" placeholder="Password" >
        <input type="password" class="form-control" name = "cpass" placeholder="Confirm Password">
        <button class="btn btn-lg btn-primary btn-block" name = "submit" type="submit">Register</button>
        <br>
        <p> <strong> Already a member? Sign in </strong> <a href = "login.php"> here </a></p>

        <?php

          include 'config.inc.php';

          if (isset($_POST['submit'])) {
            if ($_POST['pass'] == $_POST['cpass']) {
              $name = $_POST['name'];
              $email = $_POST['email'];
              $pass = $_POST['pass'];

              $salt = substr(str_replace('+', '.', base64_encode(sha1(microtime(true), true))), 0, 22);
              $pass_hash = crypt($pass, '$2a$12$'.$salt);
              $query = "INSERT INTO users(name, user_email, pass_hash, salt) VALUES('$name', '$email', '$pass_hash', '$salt')";
              $result = mysqli_query($dbconn, $query);
              if ($result) {
                session_start();
                $_SESSION['name'] = $_POST['name'];
                header("Location: home.php");
              }
            } else {
                echo "Passwords not matching!";
            }
          }
        ?>

      </form>
    </div>
  </body>
</html>
