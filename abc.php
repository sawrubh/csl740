<?php

$name = $_GET['name'];
$address = $_GET['address'];
$city = $_GET['locality'];
$website = $_GET['website'];
$email = $_GET['email'];
$lat = $_GET['lat'];
$lon = $_GET['lon'];
$tel = $_GET['tel'];
$fax = $_GET['fax'];
$catprimary = $_GET['catp'];
$cat = $_GET['cat'];

if($name == '' and $address == '' and $city == '' and $website == '' and $email == '' and $lat == '' and $lon == '' and $tel == '' and $fax == '' and $catprimary == '')
{
	session_start();
	$initial = $_SESSION["ini"];
	$current = $_SESSION["cur"];
	$List = $_SESSION["arr"];
	$maxData = $_SESSION["max"];
	$integ = $_GET["integ"];
	$ind = $initial + $integ;

	$name = $List[$ind][0];
	$address = $List[$ind][1];
	$city = $List[$ind][2];
	$website = $List[$ind][3];
	$email = $List[$ind][4];
	$lat = $List[$ind][5];
	$lon = $List[$ind][6];
	$tel = $List[$ind][7];
	$fax = $List[$ind][8];
	$cat = $List[$ind][9];
	$catprimary = $List[$ind][10];
}

switch(strtolower($catprimary))
	{
		case 'travel':
			$ctable = 'travel';
			break;
		case 'restaurant':
			$ctable = 'restaurant';
			break;
		case 'shopping':
			$ctable = 'shopping';
			break;
		case 'recreation':
			$ctable = 'recreation';
			break;
		case 'entertainment':
			$ctable = 'entertainment';
			break;
		case 'education':
			$ctable = 'education';
			break;
		default:
			$ctable = 'places';
	}

	$query = "select name,address1,address2,locality,region,website,email,latitude,longitude,tel,fax,categry2,categry1 from ".strtolower($ctable)." where 
	categry2 = '".strtolower($cat)."' order by distance(location, GeometryFromText('POINT(".$lat." ".$lon.")',2167)) limit 5";
	
	include 'config.inc.php';
	$result = mysqli_query($dbconn, $query);
	$Similar = array();
	
	while($row = mysqli_fetch_row($result))
	{
		$temp = array($row[0],$row[1]." ".$row[2],$row[3],$row[5],$row[6],$row[7],$row[8],$row[9],$row[10],$row[11],$row[12]);
		if (!(strtolower($row[0]) == strtolower($name)))
		{
			$Similar[] = $temp;
		}
	}
	$len = count($Similar);
	$i=0;
	$str = '';
	while($i<$len)
	{
		$str = $str . $Similar[$i][0] . ',' . $Similar[$i][1] . ',' . $Similar[$i][2] . ',' . $Similar[$i][3] . ',' . $Similar[$i][4] . ','. $Similar[$i][5] . ',' . $Similar[$i][6] . ',' . $Similar[$i][7] . ',' . $Similar[$i][8] . ',' . $Similar[$i][9] . ',' . $Similar[$i][10] . '%%';
		$i = $i + 1;
	}
?>

<html>
  <head>
	<link rel="stylesheet" href="style.css">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body
	  { 
					color: #262626;
					background:fixed center repeat-x;
					background-image: url(back.jpg);
					font: normal 12px/18px Verdana, sans-serif;
					background-size:1000px 800px
	  }
	  fieldset { margin: 0 0 22px 0; border: 1px solid #2B6600; padding: 12px 17px; background-color: #DAF5C7; }
	  legend { font-size: 1.1em; background-color: #2B6600; color: #FFFFFF; font-weight: bold; padding: 4px 8px; }

form br { display: inline; }
	
	#toplevel
	{
		position:absolute;
			top:0px;
			left:0px;
			width:100%; 
			height:15%; 
			border: 2px solid black;
			 background:fixed center;
			background-image: url(blue.png);
	}
	#toplevelBJS
	{
		position:absolute;
		top:10px;
		left:10px;
		font-family: "New Century Schoolbook", Times, serif;
		
	}
	#SA
	{
		position:absolute;
		top:10px;
		left:200px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#RS
	{
		position:absolute;
		top:30px;
		left:500px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#AAP
	{
		position:absolute;
		top:30px;
		left:800px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#EC
	{
		position:absolute;
		top:30px;
		left:1050px;
		font-family: "New Century Schoolbook", Times, serif;
	}
      #map_canvas 
		{
			position:absolute;
			top:110px;
			left:0px;
			width:800px; 
			height:80%; 
			border: 2px solid black;
		}
      #search_area 
		{	 
			background-image: url(green.jpg);
		}
		
	#stylized
	{
			position: absolute;
			top:100px;
			left:800px;
			width : 500px;
			height:80%;
			background-image: url(green.jpg);
			margin: 10px 10px 10px 10px; 
			border: 2px solid black; 
}

      #search_relative_1 {position: absolute; float; width = 40%; right: 40px ; top:370px; margin: 10px 10px 10px 10px; border: 2px solid black; }
      #search_relative_2 {position: absolute; float; width = 40%; right: 80px ; top:640px; margin: 10px 10px 10px 10px; border: 2px solid black;}
    </style>
	
	 <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC8NGJA6xF3MC8UxNVY1hrK8C1P9LW8has&sensor=true">
    </script>
	<script type="text/javascript">
		
		
		function capitalize(A)
		{
			var temp = A.split(' ');
			var n  = temp.length;
			var i=0;
			var res = '';
			while(i<n)
			{				
				res = res + temp[i].substring(0,1).toUpperCase() + temp[i].substring(1,temp[i].length) + ' ';
				i = i + 1;
			}
			return res;
		}
		
		function trim(A)
		{
			return A.replace(/^\s+|\s+$/g,"");
		}
      function initialize() 
	  {
		var nam = "<?php echo $name ?>";
		var add = "<?php echo $address ?>";
		var web = "<?php echo $website ?>";
		var emal = "<?php echo $email ?>";
		var lat = "<?php echo $lat ?>";
		var lon = "<?php echo $lon ?>";
		var cit = "<?php echo $city ?>";
		var la = parseFloat(lat);
		var lo = parseFloat(lon);
		var cat = "<?php echo $cat ?>";
		var fax = "<?php echo $fax ?>";
		var tel = "<?php echo $tel ?>";
		
		nam = capitalize(nam);
		add = capitalize(add);
		cit = capitalize(cit);
		cat = capitalize(cat);
		
		
		if(!(nam == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> Name: ' + nam +'</font></p></center><br />';
		}
		if(!(add == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> Address: ' + add +'</font></p></center>';
		}
		if(!(cit == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> City: ' + cit +'</font></p></center>';
		}		
		if(!(cat == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> Category: ' + cat +'</font></p></center>';
		}
		if(!(web == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> Website: ' + '<a href=' + web + '>' + web + '</a>' +'</font></p></center>';
		}
		if(!(emal == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> Email: ' + emal +'</font></p></center>';
		}
		if(!(fax == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> Fax: ' + fax +'</font></p></center>';
		}
		if(!(tel == ''))
		{
			document.getElementById('stylized').innerHTML  += '<center><p> <font size=4 color = "white"> Tel No: ' + tel +'</font></p></center>';
		}
		
		var str = '<fieldset> <legend>Similar Results:</legend>';
		
		
		var display = "<?php echo $str;?>";
		var Adisplay = display.split('%%');
		
		
		
		var len = Adisplay.length;
		for (var i=0;i<len-1;i++)
		{
			var dip = Adisplay[i].split(',');
			
			var ref = '<a href = "abc.php?name=' + trim(dip[0]) + '&address='+trim(dip[1]) + '&locality=' + trim(dip[2]) + '&website=' + trim(dip[3]) + '&email=' + trim(dip[4]) + '&lat=' + trim(dip[5]) + '&lon=' + trim(dip[6]) + '&tel=' + trim(dip[7])  + '&fax=' + trim(dip[8]) + '&cat=' + trim(dip[9]) + '&catp=' + trim(dip[10]) + '">' +  capitalize(trim(dip[0]))+ ' </a>';
			str = str + '<ul><li>' + ref +' </li></ul>';
		}
		str = str + '</fieldset>';
		
		document.getElementById('stylized').innerHTML  += str;
		
				
		
	    var myOptions = {
          center: new google.maps.LatLng(la,lo),
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          styles: [
		  {
		    featureType: "administrative",
		    elementType: "labels",
		    stylers: [
		      { visibility: "on" }
		    ]
		  },{
		    featureType: "landscape",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "poi",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "road",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "transit",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "water",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  }
		]
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"),
            myOptions);
			

	 var marker = new google.maps.Marker({
	    position: map.getCenter(),
	    map: map,
	    title: 'Click to zoom'
	  });
	  }
	   </script>
	   
	   </head>
   <body onload="initialize()">
		  <div id="map_canvas" ></div>
	<div id="toplevel">
	</div>
		<div id = "toplevelBJS">
		<br />
		<p> <font size="6" color = "white"> CSL740 </font>  </p>
		</div>
		<div id="SA">
		<br />
			<p> <font size="5" color = "white"> <a href="home.php">Search Area</a></font>  </p>
		</div>
		
		<div id="RS">
			<p> <font size="5" color = "white"> <a href="rs.php"> Relative Search </a></font>  </p>
		</div>
		
		<div id="AAP">
			<p> <font size="5" color = "white"><a href="aap.php">Around A point  </a></font>  </p>
		</div>
		
		<div id="stylized"> 
		
		
		</div>
		
		
		 </body>
</html>
