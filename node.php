<?php

class abc
{
	public $name =  '';
	public $address = '';
	public $category = '';
	public $website = '';
	public $email = '';
	public $latitud = '';
	public $longitud = '';
	
	public function initialize($n,$a,$c,$w,$e,$la,$lo)
	{
		$this -> name =  $n;
		$this -> address = $a;
		$this -> category = $c;	
		$this -> website = $w;	
		$this -> email = $e;
		$this -> latitud = $la;
		$this -> longitud = $lo;
	}
}

?>