<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body 
	  { 
					color: #262626;
					background:fixed center repeat-x;
					background-image: url(back.jpg);
					font: normal 12px/18px Verdana, sans-serif;
					background-size:1000px 800px
	  }
	  fieldset { margin: 0 0 22px 0; border: 1px solid #2B6600; padding: 12px 17px; background-color: #DAF5C7; }
	  legend { font-size: 1.1em; background-color: #2B6600; color: #FFFFFF; font-weight: bold; padding: 4px 8px; }

form br { display: inline; }
	
	#toplevel
	{
		position:absolute;
			top:0px;
			left:0px;
			width:100%; 
			height:15%; 
			border: 2px solid black;
			 background:fixed center;
			background-image: url(blue.png);
	}
	#toplevelBJS
	{
		position:absolute;
		top:10px;
		left:10px;
		font-family: "New Century Schoolbook", Times, serif;
		
	}
	#SA
	{
		position:absolute;
		top:10px;
		left:150px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#RS
	{
		position:absolute;
		top:30px;
		left:350px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#AAP
	{
		position:absolute;
		top:30px;
		left:550px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#AD
	{
		position:absolute;
		top:30px;
		left:800px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#LO
	{
		position:absolute;
		top:30px;
		left:1050px;
		font-family: "New Century Schoolbook", Times, serif;
	}
	#LI
	{
		position:absolute;
		top:30px;
		left:1050px;
		font-family: "New Century Schoolbook", Times, serif;
	}
      #map_canvas 
		{
			position:absolute;
			top:110px;
			left:0px;
			width:800px; 
			height:80%; 
			border: 2px solid black;
		}
      #search_area 
		{	 
			background-image: url(green.jpg);
		}
		
	#stylized
	{
			position: absolute;
			top:100px;
			left:800px;
			width : 500px;
			height:80%;
			background-image: url(green.jpg);
			margin: 10px 10px 10px 10px; 
			border: 2px solid black; 
}

      #search_relative_1 {position: absolute; float; width = 40%; right: 40px ; top:370px; margin: 10px 10px 10px 10px; border: 2px solid black; }
      #search_relative_2 {position: absolute; float; width = 40%; right: 80px ; top:640px; margin: 10px 10px 10px 10px; border: 2px solid black;}
    </style>
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC8NGJA6xF3MC8UxNVY1hrK8C1P9LW8has&sensor=true">
    </script>
    <script type="text/javascript">
      function initialize() {

        var myOptions = {
          center: new google.maps.LatLng(45.397, 270.644),
          zoom: 4,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          styles: [
		  {
		    featureType: "administrative",
		    elementType: "labels",
		    stylers: [
		      { visibility: "on" }
		    ]
		  },{
		    featureType: "landscape",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "poi",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "road",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "transit",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  },{
		    featureType: "water",
		    elementType: "labels",
		    stylers: [
		      { visibility: "off" }
		    ]
		  }
		]
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"),
            myOptions);

	 var marker = new google.maps.Marker({
	    position: map.getCenter(),
	    map: map,
	    title: 'Click to zoom'
	  });

	
	var clickListen = google.maps.event.addListener(map, "click", function(event)
    {
    	var x_coord = event.latLng.lat();
    	var y_coord = event.latLng.lng();
    	document.getElementById("latitude").value = x_coord;
    	document.getElementById("longitude").value = y_coord;
    });
}
    </script>
		  
		  <script language="JavaScript" type="text/javascript">
		  <!--
		  
		  
		  function checkSel(selName,selIndex,val,val2){
			  if(selName=="sel1"&&selIndex!=0){
				  document.forms["form3"]['sel'+val].disabled=false;
				  
				  
				  
				  
				  
				  switch(selIndex){
					  case 1:
						  var arr = new Array("Charter & Private Planes","Lodges & Vacation Rentals","Tourist Attractions","Hotels & Motels","Airports","Tourist Information & Services","Limos & Chauffers","Railroads & Trains","Cruises","Hostels","Taxi & Car Services","Wineries & Vineyards","Airlines","Bed & Breakfasts","Public Transportation & Transit","Monuments","Charter Buses","Resorts","Lodging","Cottages & Cabins","Travel Agents & Tour Operators","Historical Sites","Parking");
						  var i = 0;
						  document.forms["form3"]['sel'+val].length = 23
						  for(i= 0; i<23; i++)
							  document.forms["form3"]['sel'+val].options[i+1] = new Option(arr[i], arr[i]);
						  break;
					  case 2:
						  var arr = new Array("Thai","Sushi","Indian","Diners","French","Burgers","Buffets","Mexican","Chinese","Korean","Fast Food","Breweries","American","Cafes, Coffee Houses & Tea Houses","Chocolate","Ethnic Food","Vegan & Vegetarian","Italian","Pizza","Restaurants","Japanese","Cheese","Delis","Health & Diet Food","Bakeries","Farmers' Markets","Seafood","Steakhouses","Dessert","Bagels & Donuts","Barbecue","Juice Bars & Smoothies","Beer, Wine & Spirits","Middle Eastern","Ice Cream Parlors");
						  var i = 0;
						  document.forms["form3"]['sel'+val].length = 35;
						  for(i= 0; i<35; i++)
							  document.forms["form3"]['sel'+val].options[i+1] = new Option(arr[i], arr[i]);
						  break;
					  case 3:
						  var arr = new Array("Beauty Products","Bicycle Sales & Service","Flea Markets","Computers & Electronics","Video Rental","Clothing & Accessories","Toy Stores","Art Restoration","Shopping Centers & Malls","Wedding & Bridal Shops","Jewelry & Watches","Costumes","Arts & Crafts Supplies","Shoes","Gift & Novelty Stores","Hobby, Collectibles & Craft Stores","Newsstands","Gas Stations & Convenience Stores","Antiques","Bookstores","Luggage","ATMs","Supermarkets & Groceries","Furniture","Tobacco Shops","Fashion","Music","Auctions","Adult Shopping","Cards & Stationery","Department Stores","Florists","Vintage Stores & Thrift Shops","Pet Shops & Pet Supplies","Discount Stores, Warehouses & Wholesale Stores","Candy Stores","Outlet Stores","Glasses & Optometrist","Tailors","Cameras & Photos & Frames","Nurseries & Garden Centers","Children's Products","Sporting Goods","Pawn Shops");
						  var i = 0;
						  document.forms["form3"]['sel'+val].length = 41;
						  for(i= 0; i<41; i++)
							  document.forms["form3"]['sel'+val].options[i+1] = new Option(arr[i], arr[i]);
						  break;
					  case 4:
						  var arr = new Array("Parks","Boat Charters & Rentals","Swimming Pools","Sports Clubs","Race Tracks","Fairgrounds & Rodeos","Playgrounds","Hot Air Balloons","Martial Arts","Ice Skating","Gun Ranges","Rafting","Hockey","Beaches","Golf","Gyms & Fitness Centers","Racquetball","Circuses","Yoga & Pilates","Zoos, Aquariums & Wildlife Sanctuaries","Bicycle Rentals","Outdoor Recreation","Campgrounds & RV Parks","Personal Trainers","Baseball","Archery","Hunting & Fishing","Lakes","Botanical Gardens","Party Centers","Soccer Clubs & Instruction","Sports & Recreation Centers","Football","Go Carts","Water Sports","Stadiums, Arenas & Athletic Fields","Sports Instruction","Picnic Areas","Tennis","Rock Climbing","Ski & Snowboard");
						  var i = 0;
						  document.forms["form3"]['sel'+val].length = 41;
						  for(i= 0; i<41; i++)
							  document.forms["form3"]['sel'+val].options[i+1] = new Option(arr[i], arr[i]);
						  break;
					  case 5:
						  var arr = new Array("Hookah Lounges","Arcades & Amusement Parks","Karaoke","Internet Cafes","Concert Halls & Theaters","Ticket Sales","Bingo Halls","Night Clubs","Museums","Orchestras, Symphonies & Bands","Sports Bars","Gay Bars","Comedy Clubs","Jazz & Blues Cafes","Pubs and Taverns","Art Dealers & Galleries","Hotel Lounges","Casinos & Gaming","Dance Halls & Saloons","Wine Bars","Psychics & Astrologers","Bowling Alleys","Billiard Parlors & Pool Halls","Adult Entertainment","Bars");
						  var i = 0;
						  document.forms["form3"]['sel'+val].length = 25;
						  for(i= 0; i<25; i++)
							  document.forms["form3"]['sel'+val].options[i+1] = new Option(arr[i], arr[i]);
						  break;
					  case 6:
						  var arr = new Array("schools","art instruction & schools","colleges & universities","driving school","health & medicine education","dance instruction","educational services","test prep & tutors","computer training","gymnastics instruction","adult education","vocational schools","fraternities & sororities");
						  var i = 0;
						  document.forms["form3"]['sel'+val].length = 13;
						  for(i= 0; i<13; i++)
							  document.forms["form3"]['sel'+val].options[i+1] = new Option(arr[i], arr[i]);
						  break;
						  
				  }
				  
				  
				  
				  
				  
				  
			  }
			  if(selName=="sel1"&&selIndex==0){
				  document.forms["form3"]['sel'+val].disabled=true;
				  document.forms["form3"]['sel'+val].selectedIndex=0;
			  }
			  if(selName=="sel2"&&selIndex==0){
				  document.forms["form3"]['sel'+val].disabled=true;
				  document.forms["form3"]['sel'+val].selectedIndex=0;
			  }
		  }
		  </script>


   </head>
   <body onload="initialize()">
	<div id="map_canvas" ></div>
	<div id="toplevel">
	</div>
		<div id = "toplevelBJS">
		<br />
		<p> <font size="6" color = "white">CSL740</font>  </p>
		</div>
		<div id="SA">
		<br />
			<p> <font size="5" color = "white"> <a href="home.php">Search Area</a></font>  </p>
		</div>
		
		<div id="RS">
			<p> <font size="5" color = "white"> <a href="rs.php"> Relative Search </a></font>  </p>
		</div>
		
		<div id="AAP">
			<p> <font size="5" color = "white"><a href="aap.php">Around A point  </a></font>  </p>
		</div>
        
        <div id="AD">
			<p> <font size="5" color = "white"><a href="ad.php">Search Events  </a></font>  </p>
		</div>
		
        <?php
		session_start();
		if (isset($_SESSION['name'])) {
        $firstName = explode(' ', trim($_SESSION['name']));
		echo "<div id='LO'>
			    <p> <font size='5' color = 'white'> Yo, ".$firstName[0]." <a href='logout.php'>Logout</a></font>  </p>
        	  </div>";
		} else {
		echo "<div id='LI'>
			    <p> <font size='5' color = 'white'> <a href='login.php'>Login</a></font>  </p>
        	  </div>";
		}
		?>
		
<div id="stylized">
	<br />
    <center> <p> <font size="6" color = "white"> Select a point and <br>search around it </font> </p> </center>
	<br />
    <form id="form3" method="post" action="form3-handle.php">
		  
		  <fieldset>
		  
		  <legend>Looking For:</legend>	
		  <div>
		  <b>Category : </b><select name="sel1" onchange="checkSel(this.name,this.selectedIndex,2,3)">
		  <option>select</option>
		  <option>Travel</option>
		  <option>Restaurant</option>
		  <option>Shopping</option>
		  <option>Recreation</option>
		  <option>Entertainment</option>
		  <option>Education</option>
		  </select><br />		
		  <br />					
		  
		  
		  
		  <b>Sub-Category: </b><select name="sel2" disabled >
		  <option>select</option>
		  <option>blah2_1</option>
		  <option>blah2_2</option>
		  <option>blah2_3</option>
		  </select><br />
		  </div>
		  
		  </fieldset>
		  <br/>
		  
		  
		  
	<fieldset>
		<legend>Press on Map: </legend>	
			<font size="3"> Latitude: </font> <input id="latitude" name="latitude" type="text">
			<br />
			<br />
			
			<font size="3"> Longitude: </font> <input id="longitude" name="longitude" type="text">
			<br/>
			<br/>
			<font size="3"> Distance: </font><input name="distance" type="text"><br>
			<br/>
	</fieldset>
  
    <center> <input type="submit"> </center>
    </form>
    </div>
  </body>
</html>
